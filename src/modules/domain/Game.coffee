'use strict'

###
Example JSON returned by the server

  "starbound": {
    "ports": [21025,21026],
    "running": false,
    "ready": false,
    "players": [],
    "version": ""
  },
###

class Game
  constructor: (@name, @ports)->
    @running = @ready = false
    @players = []

Game.fromJson = (name, json)->
  game = new Game(name, json.ports)

  game.running = !!json.running
  game.ready = !!json.ready

  return game

module.exports = Game
