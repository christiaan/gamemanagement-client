'use strict'
assert = require 'assert'
Q = require 'q'
Game = require './Game'

describe 'Game', ->
  describe 'constructor', ->
    it 'should have the name with which it is constructed', ->
      name = 'some name'
      game = new Game(name, [])
      assert.equal game.name, name
    it 'should have the ports it has been assigned', ->
      ports = [123, 1234]
      game = new Game('', ports)
      assert.equal game.ports, ports
    it 'should not be running nor ready', ->
      game = new Game '', []
      assert.ok !game.running
      assert.ok !game.ready

  describe 'fromJson', ->
    it 'should succesfully construct a Game from a valid object', ->
      obj =
        ports : [123, 456]
        running: true
        ready: false
        players: []
        version: ""

      name = 'game name'
      game = Game.fromJson(name, obj)

      assert.equal(game.name, name)
      assert.equal(game.ports, obj.ports)
      assert.equal(game.running, obj.running)
      assert.equal(game.ready, obj.ready)
