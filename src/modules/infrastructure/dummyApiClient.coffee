'use strict'
require 'angular'
Game = require '../domain/Game'

json = {
  "starbound": {
    "ports": [21025,21026],
    "running": false,
    "ready": false,
    "players": [],
    "version": ""
  },
  "terraria": {
    "ports": [7777],
    "running": true,
    "ready": true,
    "players": [],
    "version": "v1.2.4.1"
  }
}

games = []
angular.forEach json, (value, name)-> games.push Game.fromJson(name, value)

module.exports = ($q)->
  {
    listGames: -> $q.when(games)
    startGame: (game)-> $q.when("OK")
    stopGame: (game)-> $q.when("OK")
  }
