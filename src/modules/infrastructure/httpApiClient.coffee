'use strict'
Game = require '../domain/Game'

module.exports = ($http, $q)->
  {
    listGames: ->
      $q (resolve, reject)->
        promise = $http.get '/games/list.json', {responseType: 'json'}
        promise.success (json)->
          games = []
          angular.forEach json, (value)->
            games.push Game.fromJson(value)
          resolve games

        promise.error reject

    startGame: (game)->
      $q (resolve, reject)->
        $http.get('/games/' + game.name + '/start', {responseType: 'json'})
        .success(resolve)
        .error(reject)


    stopGame: (game)->
      $q (resolve, reject)->
        $http.get('/games/' + game.name + '/stop', {responseType: 'json'})
        .success(resolve)
        .error(reject)
  }
