'use strict'
assert = require 'assert'
Q = require 'q'
GameListController = require './GameListController'

describe 'GameListController', ->
  describe 'initial state', ->
    it 'should have the loading property when loading', (done)->
      games = []
      httpApiClient = { listGames : -> Q(games) }

      controller = new GameListController(httpApiClient)
      assert.ok(controller.games.loading, 'Initial state should be loading')
      setImmediate ->
        assert.ok(
          !controller.games.loading
          'Next tick the loading flag should be gone'
        )
        assert.equal(
          controller.games
          games
          'Games is updated on next tick'
        )
        done()
