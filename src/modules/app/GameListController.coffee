'use strict'

class GameListController
  constructor: (httpApiClient)->
    @games = []
    @games.loading = true
    httpApiClient.listGames().then((games)=>
      @games = games
    )

module.exports = GameListController
