'use strict'

GameListController = require './GameListController'

module.exports = angular.module('gamemanagement-client', [
  'ngRoute'
  require('../../../tmp/templates').name
  require('../common').name
  require('../infrastructure').name
])
.controller('GamesListController', ['httpApiClient', GameListController])
.config(['$routeProvider', ($routeProvider)->
  $routeProvider.when(
    '/list'
    {
      templateUrl: 'app/game_list.html'
      controller: 'GamesListController'
      controllerAs: 'gamesList'
    }
  )
  $routeProvider.otherwise({redirectTo: '/list'})
])
