'use strict'

module.exports = angular.module('gamemanagement-client.common', [
  require('./directives/index').name
  require('./filters/index').name
  require('./services/index').name
])
