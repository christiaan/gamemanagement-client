'use strict';

var gulp = require('gulp');
var mocha = require('gulp-mocha');
require('coffee-script');

module.exports = gulp.task('test', function () {
  return gulp.src(config.paths.src.tests, {read: false})
    .pipe(mocha());
});
